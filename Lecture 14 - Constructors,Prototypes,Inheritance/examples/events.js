/* var parent = {
	name: "Petro",
	age: "45"
},

child = Object.create(parent, {
  // foo is a regular 'value property'
  foo: {
  	writable: true,
  	configurable: true,
  	value: 'hello',
  	enumerable: false
  },
  // bar is a getter-and-setter (accessor) property
  age1: {
  	value: 42,
  	enumerable: true
  }
});

child.name = "Vasya";

console.log(child);


for(var key in child){
	console.log(key);
} */


function Person(name) {
  this.name = name;
}

Person.prototype.sayName = function () {
  console.log(`My name is ${this.name}`);
};

Person.prototype.walk = function () {
  console.log('I can walk!');
};


function Student(name, subject) {
  Person.call(this, name);

  this.subject = subject;
}

Student.prototype = Object.create(Person.prototype);
Student.prototype.constructor = Student;

// Student.prototype.sayName = function () {
//   console.log(`I'm a student. My name is ${this.name}`);
// };

// Student.prototype.saySubject = function () {
//   console.log(`I'm a student. My subject is ${this.subject}`);
// };

let per = new Person('Petro'),
  stud = new Student('Vasya', 'Math');


// Student.prototype.sayHello = function () {
//   console.log('Hello');
// };

console.log('Person', per);
console.log('Person', stud);
