const xhr = new XMLHttpRequest();

xhr.open('GET', 'data.json', true);

xhr.onload = function(event) {
    const { status } = this;
    if (status === 200) {
        console.log(this)
        console.log(this.responseText)
        console.log(JSON.parse(this.responseText))
    } else {
        console.error(this.status, this.statusText);
    }
}

xhr.send();